.DEFAULT_GOAL := all

#############
# Variables #
#############

EXECUTABLE := spry

ifdef GOOS
	EXECUTABLE := $(EXECUTABLE)-$(GOOS)
endif

ifdef GOARCH
	EXECUTABLE := $(EXECUTABLE)-$(GOARCH)
endif

VERSION ?= $(shell ./.version/calculate-version.sh)

###########
# TARGETS #
###########

$(EXECUTABLE):
	go build -ldflags="-extldflags=-static -X main.Version=$(VERSION)" -o $(EXECUTABLE) .

.PHONY: clean-build
clean-build:
	rm -f $(EXECUTABLE)

.PHONY: build
build: clean-build $(EXECUTABLE)

.PHONY: all
all: build
