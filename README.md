# spry

Basic [Sprig](https://masterminds.github.io/sprig/) &
[Helm Templating Language (HTL)](https://helm.sh/docs/howto/charts_tips_and_tricks/#know-your-template-functions)
Template Rendering Tool.

It's design emulates how the `helm template` command runs, for rendering simple
Go Templates, Sprig extended Go Templates, or Helm Templating Language (HTL)
Templates, outside of a Helm Chart.

It takes provided templates and renders them using provided values.

### _ Files

Like with `helm template` template files whose names begins with `_` will not
produce output, but are used to provide common code used in other template
files.

## Usage Examples

```bash
$ ./spry
Usage of ./spry [Options] [Template Files]:
  -d string
        Output delimiter between template files rendered (default "---")
  -delimErr
        Whether to print output delimiter to STDERR.
  -f value
        Specify a values file (JSON or YAML)
  -filenameErr
        Whether to print filename to STDERR. (default true)
  -fp string
        Prefix for the filename in the output. (default "# ")
  -helm
        Use the Helm Templating Language (HTL) (a superset of Sprig).
  -helmDNS
        When using HTL, support DNS resolution. (default true)
  -helmLin
        When using HTL, enable the Linting Mode.
  -helmNestValues
        When using HTL, Nest provided values under '.Values' (default true)
  -helmStrict
        When using HTL, use strict rendering.
  -set value
        Set a specific value (foo.bar=spam)
  -v    Print the version and exit.
  -version
        Print the version and exit.

Template Files can also be directories to recurse for templates.

If no template files are provided it will attempt read from STDIN.
If no input is available from STDIN it will print this usage message instead.
Pass only '-' to Template Files to force waiting for input.

Helm Mode: use the Helm Templating Language/Engine
with the following caveats:
        - 'lookup' function unavailable.
```
